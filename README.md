# LAVA Tests Resource Monitoring

This repository contains documentation and examples on how to write tests that can
be used to track and report system resources during automated tests execution
in LAVA.

## Which resources can be monitored?

This document focuses on tracking local system resources during the whole test execution.
Multiple metrics can be captured for a single test run.
Due to LAVA limitation only a single value for each metric can be captured.

A non-exhaustive list of metrics that can be tracked are:
* total execution time 
* total CPU consumption
* maximum RAM resident-set size
* bandwidth consumption
* number of context switches

Custom tools can be used to provide any desired metric supported on Linux.

## How should I structure my tests to collect the needed information?

The test to be monitored must be a single executable, either a compiled binary
or an interpreted script.

The LAVA test definition must then wrap the test program invocation with a
wrapper program that invokes the test programs while handling the tracking,
parsing and reporting of the system resources stats.

The section further below provide the details of how this can be done.

## How do I check the results?

The collected metrics will be captured as LAVA measurements, each in its own
testcase result separate from the testcase result of the original test program.

For instance, when running the `test-monitoring-with-limit` test, the LAVA
output includes the following testcase results:
* test-monitoring-with-limit: the original testcase, with its own pass/fail value
* cpu_percentage: captures the average percentage of CPU used by the test 
* elapsed_real_time: the execution time for the test
* maximum_resident_set_size: the maxmimum RSS during test execution, marked as
  failed if it passes a certain threshold

The captured metrics can be analyzed in LAVA results page.

Further analytic capabilities can be provided by extending the QA report service
to provide charts and diagrams alongside the test results: 

https://gitlab.apertis.org/infrastructure/qa-report-app/

## Approach

Since running a test in LAVA just basically consists on executing a test program
from a command line and processing its output to show the tests results, this
solution is flexible enough and perfectly fits the current LAVA infrastructure.

The approach taken in this solution will measure a single value for each metric
and not a full profile during the test program execution. This single value will
then be reported to LAVA as part of the executed test case and this data can be
processed like any other test result by LAVA and by any available tool or framework
that makes use of LAVA tests results.

## Prerequisites

In this repository we will explain the procedure using a test example program, and
some familiarity with writing LAVA tests definitions is required:

https://lava.collabora.co.uk/static/docs/v2/writing-tests.html

## Steps

   1. Identify the program that will be used to monitor system resources.
      In our example, we will use the `time` command to monitor the CPU,
      memory and time consumed by the test program.

   2. Create a wrapper script that will be executed directly by LAVA, and which,
      in turn, will execute the required test program while monitoring the required
      system resources.

   3. The wrapper script should use the `lava-test-case` script to record
      the monitored information from the test program as explained at:

      https://lava.collabora.co.uk/static/docs/v2/writing-tests.html#recording-test-case-measurements-and-units

   4. The main wrapper script should be executed in LAVA also using the
     `lava-test-case` script.

## LAVA and Metrics

The tool to send the metrics and tests results to LAVA is the `lava-test-case`
script. This script is already available from the `DUT` during the test execution,
so there is no need to install any package for it.

The `--measurement` option of this script can be used to record metrics, and
the required parameters for the script are the `name`, `measurement` and `units`
of the test case.

For example, in the case of this repository example, the `time` command is used
to get some stats for the main test program.

So the wrapper script executes the `time` command invoking the main test program,
it records the metric stats into `stderr` (and let the main test program to
show its output in the `stdout`), then parses each of the `stderr` lines looking
for the pattern that matches the `time` command output, in our case, this pattern
is of the form `test_case_name:metric_value:metric_unit`, then invokes the
`lava-test-case` command with these values to record the metrics in LAVA.

In this repository example, the `lava-test-case` command is invoked with something
like this for each reported metric:

```
$ lava-test-case maximum_resident_set_size --result pass --measurement 3648.000000 --units kbs
```

This will record into LAVA a test case with name `maximum_resident_set_size`,
result `pass`, and metric value `3648.000000` with units `kbs`.

All of these parameters are required for the `lava-test-case` command, so every
metric test result should always contains each of these values.

Further information about the `lava-test-case` script can be found at:

https://lava.collabora.co.uk/static/docs/v2/writing-tests.html

## Wrapper Script Design

To summarize, the wrapper script should:

   - Run the main test program.

   - Monitor (more likely with the help of other command) the system resources
     during the test program execution.

   - Parse the output generated from the command monitoring the system resources
     into the required LAVA format.

   - Use the `measurement` option from the `lava-test-case` script to record the
     monitored information into LAVA.

   - Implement any metric constraint or limit then report the test result as
     `fail` or `pass` accordingly using the `lava-test-case` script when recording
     the results to LAVA.

## Logging Monitoring Data

The approach used in this repository `run-test.py` example is to log the
monitoring program output to the standard error (stderr) , so the main test
program can continue using the standard output (stdout) during the test execution.

Once the test program finishes executing, the wrapper script reads the monitoring
logs from the standard error and parses this information into valid values that
can be passed to LAVA using the `lava-test-case` command.

Other approaches can be used, for example, get the monitoring program to log
data into a file and then read this file with the wrapper script to process its
information.

## Test Measurement Results

Since the measurement data is available in LAVA like any other test case result,
it can be accessed directly from the LAVA web user interface. For example:

https://lava.collabora.co.uk/results/1772228/3_test-monitoring

And an example setting a metric limit in the test:

https://lava.collabora.co.uk/results/1777124/4_test-monitoring-with-limit

## Example Files

   - `run-test.py`: Wrapper script implementing a couple of classes showing how
     to execute the main test program and monitor program, parse the monitored
     information and record the results into LAVA with an optional metric limit
     for one of the test execution.

   - `test-monitoring.yaml`: LAVA test definition file executing the wrapper script
     from LAVA.

   - `test-monitoring-with-limie.yaml`: LAVA test definition file executing the
     wrapper script showing how to restrain metrics results to specific values.

## Wrapper Script Example

This section explains the key components of the `run-test.py` example that should
be implemented by all scripts recording metrics into LAVA. These components can
be designed in the most suitable way for each project, tools or test case, but
they should basically always resorts to the use of two main components, the
`parser` and the `runner`.

The `run-test.py` script implements a main `LavaTestCase` class with the
`lava-test-case` command as attribute that will be used to record the metrics
results in LAVA.

This class also contains two relevant methods: `_parse` and `run`.

`_parse`: Implements the parser for the monitoring tool. It reads the monitoring
tool results from the standard error (this information can also be read from
somewhere else , like a file or in-memory object) and extracts the required values
`name` , `measurement` and `units` for every metric that are later passed to the
`lava-test-case` script.

`run`: This method just handles running the main test program with the monitoring
program to collect metric information, and then it invokes the parser to analyse
the information and record the metric and tests values into LAVA.
